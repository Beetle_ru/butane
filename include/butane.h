#ifndef __BUTANEH__
#define __BUTANEH__

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <fcntl.h>
#include <string.h>

#define UDP_MAX_SIZE 65507
#define IP_ADDRESS_SIZE 15

struct ConnectInfo {
	char *ip;
	int port;
	int sockfd;
	void *listener;
	pthread_t thread; // for controlling thread
};

struct Message {
	char *buffer;
	int length;
};

typedef void (*listenerCallback)(Message msg, struct ConnectInfo ci);

struct ConnectInfo *bInit();
struct ConnectInfo *bInit(const char *ip, int port);

int bSend(struct ConnectInfo *ci, struct Message msg);
int bSend(struct ConnectInfo *ci, char *ip, int port, struct Message msg);

int bBind(struct ConnectInfo *ci, char *listenIp, int listenPort); // bind prot(create server)

int bSetListener(struct ConnectInfo *ci, listenerCallback listener);

// tools
int ipToString(char *strIp, int ip);

void *receivThread(void *arg);

#endif // __BUTANEH__
