#include <stdio.h>
#include "butane.h"

void serverListener(Message msg, struct ConnectInfo ci);
void clientListener(Message msg, struct ConnectInfo ci);

int main() {
	struct ConnectInfo *server = bInit();
	struct ConnectInfo *client = bInit("127.0.0.1", 32000);
	
	bBind(server, "0.0.0.0", 32000);
	bSetListener(server, serverListener); 
	
	bSetListener(client, clientListener);
	
	Message msg;
	msg.buffer = "good begun is half done";
	msg.length = strlen(msg.buffer);
	
	bSend(client, msg);

	printf("press \"Enter\" for exit\n");
	getchar();

	return 0;
}


void serverListener(Message msg, struct ConnectInfo ci) {
	printf("request -> \"%s\"; length = %d\n", msg.buffer, msg.length);
	Message resp;
	resp.buffer = "ok";
	resp.length = strlen(resp.buffer);
	bSend(&ci, resp);
}

void clientListener(Message msg, struct ConnectInfo ci) {
	printf("response -> \"%s\"; length = %d\n", msg.buffer, msg.length);
}
