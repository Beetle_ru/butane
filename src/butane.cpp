#include "butane.h"

struct ConnectInfo *bInit() {
	struct ConnectInfo *ci = (struct ConnectInfo*) malloc(sizeof(struct ConnectInfo));

	ci->ip = (char*)malloc(sizeof(char) * IP_ADDRESS_SIZE);
	ci->sockfd = socket(AF_INET,SOCK_DGRAM,0);
	return ci;
}

struct ConnectInfo *bInit(const char *ip, int port) {
	struct ConnectInfo *ci = (struct ConnectInfo*) malloc(sizeof(struct ConnectInfo));

	ci->ip = (char*)malloc(sizeof(char) * IP_ADDRESS_SIZE);
	strcpy(ci->ip, ip);
	ci->port = port;
	ci->sockfd = socket(AF_INET,SOCK_DGRAM,0);
	return ci;
}

int bSend(struct ConnectInfo *ci, struct Message msg) {
	struct sockaddr_in servaddr;

	bzero(&servaddr,sizeof(servaddr));
	servaddr.sin_family = AF_INET;
	servaddr.sin_addr.s_addr=inet_addr(ci->ip);
	servaddr.sin_port=htons(ci->port);

	return sendto(
		ci->sockfd, msg.buffer, msg.length,0,
		(struct sockaddr *)&servaddr,sizeof(servaddr)
	);
}

int bSend(struct ConnectInfo *ci, char *ip, int port, struct Message msg) {
	for (int i = 0; i < IP_ADDRESS_SIZE; i++) ci->ip[i] = 0;

	strcpy(ci->ip, ip);
	ci->port = port;

	return bSend(ci, msg);
}

int bBind(struct ConnectInfo *ci, char *listenIp, int listenPort) {
	struct sockaddr_in servaddr;

	bzero(&servaddr,sizeof(servaddr));
	servaddr.sin_family = AF_INET;
	servaddr.sin_addr.s_addr=inet_addr(listenIp);
	servaddr.sin_port=htons(listenPort);

	if (bind(ci->sockfd, (struct sockaddr *)&servaddr, sizeof(servaddr))) {
		perror("ERROR: datagram socket don't binded");
		return 1;
	}
	return 0;
}

int bSetListener(struct ConnectInfo *ci, listenerCallback listener) {
	ci->listener = (void*)listener;
	if ( pthread_create(&ci->thread, NULL, receivThread, ci) != 0) {
		perror("ERROR: receive thread don't created\n");
		return 1;
	}
	return 0;
}

void *receivThread(void *arg) {
	struct ConnectInfo *ci = (struct ConnectInfo*) arg;
	listenerCallback listener = (listenerCallback) ci->listener;

	while(1) {
		struct Message msg;
		struct ConnectInfo ciFrom;
		struct sockaddr_in cliaddr;
		socklen_t structLen;
		
		structLen = sizeof(sockaddr_in);
		msg.buffer = (char*)malloc(sizeof(char) * UDP_MAX_SIZE);
		msg.length = recvfrom(
			ci->sockfd, 
			msg.buffer, 
			UDP_MAX_SIZE, 
			0, 
			(struct sockaddr *)&cliaddr, 
			&structLen
		);

		ciFrom.ip = (char*)malloc(sizeof(char) * IP_ADDRESS_SIZE);
		ipToString(ciFrom.ip, cliaddr.sin_addr.s_addr);
		ciFrom.port = htons(cliaddr.sin_port);
		ciFrom.sockfd = ci->sockfd;

		(*listener)(msg, ciFrom);

		free(msg.buffer);
		free(ciFrom.ip);
	}
}

int ipToString(char *strIp, int ip) {
	unsigned char *c = (unsigned char *) &ip;
	sprintf(strIp, "%d.%d.%d.%d", c[0], c[1], c[2], c[3]);
	return 0;
} 
