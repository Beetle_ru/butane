CMPL=g++
INCLUDE=include
SRC=src
OBJ=obj
LIB=lib
BIN=bin

LIBDEPEND=$(OBJ)/butane.o


all:libbutane.a example

libbutane.a: $(LIBDEPEND) mkdirlib
		ar rc $(LIB)/libbutane.a $(LIBDEPEND)
		ranlib $(LIB)/libbutane.a


example: $(OBJ)/example.o libbutane.a mkdirbin
		$(CMPL)   $(OBJ)/example.o -L./$(LIB) -lbutane -lpthread -o  $(BIN)/example

$(OBJ)/%.o: $(SRC)/%.cpp mkdirobj
		$(CMPL) -c $< -o $@ -I$(INCLUDE)
	       

mkdirobj:
		mkdir -p $(OBJ)

mkdirlib:
		mkdir -p $(LIB)
				
mkdirbin:
		mkdir -p $(BIN)
		
clean:
		rm -R $(OBJ) $(LIB) $(BIN)
		




